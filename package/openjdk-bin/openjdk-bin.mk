################################################################################
#
# host-openjdk-bin
#
################################################################################

HOST_OPENJDK_BIN_VERSION_MAJOR = 11.0.2
HOST_OPENJDK_BIN_VERSION_MINOR = 7
HOST_OPENJDK_BIN_VERSION = $(HOST_OPENJDK_BIN_VERSION_MAJOR)_$(HOST_OPENJDK_BIN_VERSION_MINOR)
HOST_OPENJDK_BIN_SOURCE = OpenJDK11U-jdk_x64_linux_hotspot_$(HOST_OPENJDK_BIN_VERSION).tar.gz
HOST_OPENJDK_BIN_SITE = https://github.com/AdoptOpenJDK/openjdk11-binaries/releases/download/jdk-$(HOST_OPENJDK_BIN_VERSION_MAJOR)%2B$(HOST_OPENJDK_BIN_VERSION_MINOR)
HOST_OPENJDK_BIN_LICENSE = GPLv2+ with exception
HOST_OPENJDK_BIN_LICENSE_FILES = legal/java.prefs/LICENSE legal/java.prefs/ASSEMBLY_EXCEPTION

# If the files are installed to $(HOST_DIR)/bin and $(HOST_DIR)/lib
# the build will fail because the RPATH of java binaries do not have a proper
# RPATH
define HOST_OPENJDK_BIN_INSTALL_CMDS
	mkdir -p $(HOST_DIR)/openjdk
	cp -aLrf $(@D)/* $(HOST_DIR)/openjdk/
endef

$(eval $(host-generic-package))
